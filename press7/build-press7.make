api = 2
core = 7.x

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "https://github.com/pressflow/7/tarball/master"

includes[] = press7-modules.make
includes[] = press7-themes.make
includes[] = press7-libraries.make
