api = 2
core = 7.x

;****************************************
; Libraries
; Must be listed here: http://drupal.org/packaging-whitelist
;****************************************

; Backbone
libraries[backbone][directory_name] = backbone
libraries[backbone][download][type] = get
libraries[backbone][download][url] = https://github.com/documentcloud/backbone/archive/master.zip
libraries[backbone][overwrite] = TRUE

; Chosen
libraries[chosen][directory_name] = chosen
libraries[chosen][download][type] = get
libraries[chosen][download][url] = https://github.com/harvesthq/chosen/releases/download/1.4.2/chosen_v1.4.2.zip
libraries[chosen][overwrite] = TRUE

; Colorbox
libraries[colorbox][directory_name] = colorbox
libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/tarball/master
libraries[colorbox][overwrite] = TRUE

; Ckeditor
;libraries[ckeditor][directory_name] = ckeditor
;libraries[ckeditor][download][type] = svn
;libraries[ckeditor][download][url] = http://svn.ckeditor.com/CKEditor/releases/stable/
;libraries[ckeditor][overwrite] = TRUE

; FitVids
libraries[fitvids][directory_name] = fitvids
libraries[fitvids][download][type] = get
libraries[fitvids][download][url] = https://github.com/davatron5000/FitVids.js/archive/master.zip
libraries[fitvids][overwrite] = TRUE

; Flexslider
;libraries[flexslider][directory_name] = flexslider
;libraries[flexslider][download][type] = get
;libraries[flexslider][download][url] = https://github.com/woothemes/FlexSlider/archive/flexslider1.zip
;libraries[flexslider][overwrite] = TRUE

; GeoPHP
;libraries[geophp][directory_name] = geophp
;libraries[geophp][download][type] = get
;libraries[geophp][download][url] = https://github.com/downloads/phayes/geoPHP/geoPHP.tar.gz
;libraries[geophp][overwrite] = TRUE

; Geshi Filter
;libraries[geshi_puppet][directory_name] = geshi
;libraries[geshi_puppet][download][type] = get
;libraries[geshi_puppet][download][url] = https://raw.github.com/jasonhancock/geshi-language-files/7fd7a709d857f74b78d42990a2381a45eeb93429/puppet.php
;libraries[geshi_puppet][overwrite] = TRUE

; icalcreator
;libraries[icalcreator][directory_name] = iCalcreator
;libraries[icalcreator][download][type] = get
;libraries[icalcreator][download][url] = http://kigkonsult.se/downloads/dl.php?f=iCalcreator-2.16.6
;libraries[icalcreator][overwrite] = TRUE

; JPlayer
libraries[jplayer][download][type] = "file"
libraries[jplayer][download][url] = https://github.com/happyworm/jPlayer/archive/2.1.0.zip
libraries[jplayer][subdir] = "audioplayers"

; Jquery Simple Color
libraries[jquery-simple-color][directory_name] = jquery-simple-color
libraries[jquery-simple-color][download][type] = get
libraries[jquery-simple-color][download][url] = https://github.com/recurser/jquery-simple-color/archive/master.zip
libraries[jquery-simple-color][overwrite] = TRUE

; JSON
libraries[json2][directory_name] = json2
libraries[json2][download][type] = file
libraries[json2][download][url] = https://raw.github.com/douglascrockford/JSON-js/master/json2.js
;libraries[json2][overwrite] = TRUE

; JSON Path
;libraries[feeds_jsonpath_parser][download][type] = get
;libraries[feeds_jsonpath_parser][download][url] = http://jsonpath.googlecode.com/files/jsonpath-0.8.1.php
;libraries[feeds_jsonpath_parser][destination] = modules/contrib
;libraries[feeds_jsonpath_parser][install_path] = sites/all

; Markitup
;libraries[markitup][download][type] = get
;libraries[markitup][download][url] = https://github.com/markitup/1.x/tarball/master
;libraries[markitup][patch][1715642] = http://drupal.org/files/1715642-adding-html-set-markitup-editor.patch
;libraries[markitup][overwrite] = TRUE

; Masonry
libraries[masonry][download][type] = get
libraries[masonry][download][url] = https://masonry.desandro.com/v2/jquery.masonry.min.js
libraries[masonry][overwrite] = TRUE

; MediaElement
libraries[mediaelement][directory_name] = mediaelement
libraries[mediaelement][download][type] = get
libraries[mediaelement][download][url] = https://github.com/johndyer/mediaelement/zipball/2.10.3
libraries[mediaelement][overwrite] = TRUE

; Modernizr
libraries[modernizr][directory_name] = modernizr
libraries[modernizr][download][type] = get
libraries[modernizr][download][url] = https://github.com/Modernizr/Modernizr/archive/v2.7.1.zip
libraries[modernizr][overwrite] = TRUE

; Pie, CSS Pie
;libraries[pie][directory_name] = PIE
;libraries[pie][download][type] = get
;libraries[pie][download][url] = http://cloud.github.com/downloads/lojjic/PIE/PIE-1.0.0.zip
;libraries[pie][overwrite] = TRUE

; Plupload library
libraries[plupload][directory_name] = plupload
libraries[plupload][download][type] = file
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/v1.5.8.zip
libraries[plupload][overwrite] = TRUE

; SimplePie RSS parser
libraries[simplepie][directory_name] = simplepie
libraries[simplepie][download][type] = get
libraries[simplepie][download][url] = https://github.com/simplepie/simplepie/archive/1.5.1.zip
libraries[simplepie][overwrite] = TRUE

; Sound Manager 2
libraries[soundmanager2][download][type] = "file"
libraries[soundmanager2][download][url] = https://github.com/scottschiller/SoundManager2/zipball/master
libraries[soundmanager2][subdir] = "audioplayers"

; Superfish
libraries[superfish][directory_name] = superfish
libraries[superfish][download][type] = file
libraries[superfish][download][url] = https://github.com/mehrpadin/Superfish-for-Drupal/zipball/master
;libraries[superfish][overwrite] = TRUE

; TinyMCE
;libraries[tinymce][directory_name] = tinymce
;libraries[tinymce][download][type] = get
;libraries[tinymce][download][url] = http://github.com/downloads/tinymce/tinymce/tinymce_3.4.9_jquery.zip
;libraries[tinymce][overwrite] = TRUE

; Underscore
libraries[underscore][directory_name] = underscore
libraries[underscore][download][type] = get
libraries[underscore][download][url] = https://github.com/documentcloud/underscore/archive/master.zip
libraries[underscore][overwrite] = TRUE

; XSPF Slim Player
libraries[player][download][type] = "file"
libraries[player][download][url] = http://downloads.sourceforge.net/project/musicplayer/musicplayer/slim-player-0.2.3b/xspf_player_slim-correct-0.2.3.zip
libraries[player][subdir] = "audioplayers"
