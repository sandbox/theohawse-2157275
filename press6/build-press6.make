core = 6.x
api = 2

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "https://github.com/pressflow/6/tarball/master"

includes[] = press6-modules.make
includes[] = press6-libraries.make
includes[] = press6-themes.make
