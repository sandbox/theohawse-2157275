api = 2
core = 6.x

;****************************************
; Libraries
;****************************************

; Absolute URL for feeds image grabber
libraries[url_to_absolute][download][type] = "get"
libraries[url_to_absolute][download][url] = "http://downloads.sourceforge.net/project/absoluteurl/absoluteurl/1.6/AbsoluteUrl.zip?r=&ts=1329521877&use_mirror=cdnetworks-us-2"
libraries[url_to_absolute][directory_name] = "absoluteurl"

; Boilerplate
libraries[html5bp][download][type] = "file"
libraries[html5bp][download][url] = "http://github.com/paulirish/html5-boilerplate/zipball/v1.0stripped"

; CKEditor
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.1/ckeditor_3.6.1.tar.gz"

; Colorbox
libraries[colorbox][type] = "libraries"
libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "http://cdn.jsdelivr.net/colorbox/1.3.18/colorbox.zip"

; Dompdf
libraries[dompdf][download][type] = git
libraries[dompdf][download][url] = https://github.com/dompdf/dompdf.git
libraries[dompdf][download][tag] = v0.6.0-b3

; Getid3
libraries[getid3][download][type] = "get"
libraries[getid3][download][url] = "http://downloads.sourceforge.net/project/getid3/getID3%28%29%201.x/1.7.9/getid3-1.7.9.zip"
libraries[getid3][directory_name] = "getid3"
libraries[getid3][patch][] "https://www.drupal.org/files/id3-remove-demos-1.patch"

; Head.js
libraries[headjs][download][type] = git
libraries[headjs][download][url] = https://github.com/headjs/headjs.git
libraries[headjs][download][tag] = v0.96

; HTML Purifier
libraries[htmlpurifier][download][type] = "get"
libraries[htmlpurifier][download][url] = "http://htmlpurifier.org/releases/htmlpurifier-4.5.0-lite.zip"
libraries[htmlpurifier][directory_name] = "htmlpurifier"

; Jplayer
;libraries[jplayer][download][type] = "get"
;libraries[jplayer][download][url] = "http://www.jplayer.org/1.2.0/jQuery.jPlayer.1.2.0.zip"
;libraries[jplayer][directory_name] = "jplayer"

; JQuery UI
libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "https://github.com/jquery/jquery-ui/archive/1.12.1.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"
libraries[jquery_ui][destination] = "libraries"

; JQuery Update
libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"

; Superfish
libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal/archive/master.zip"
libraries[superfish][directory_name] = "superfish"

; Tcpdf
;libraries[tcpdf][download][type] = "get"
;libraries[tcpdf][download][url] = "http://downloads.sourceforge.net/project/tcpdf/tcpdf_6_0_051.zip"
;libraries[tcpdf][directory_name] = "tcpdf"

; TinyMCE
libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "https://github.com/tinymce/tinymce-dist/archive/4.7.7.zip"
libraries[tinymce][directory_name] = "tinymce"

;Soundmanager2
libraries[soundmanager2][download][type] = "get"
libraries[soundmanager2][download][url] = "https://github.com/scottschiller/SoundManager2/tarball/master"
libraries[soundmanager2][directory_name] = "soundmanager2"
