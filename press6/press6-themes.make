api = 2
core = 6.x

;****************************************
; Themes
;****************************************

projects[sky][version] = 3.11
projects[sky][type] = "theme"

projects[tao][version] = 3.2
projects[tao][type] = "theme"

projects[roots][version] = 1.0
projects[roots][type] = "theme"

projects[adaptivetheme][version] = 3.0-rc3
projects[adaptivetheme][type] = "theme"

projects[acquia_prosper][version] = 1.1
projects[acquia_prosper][type] = "theme"

projects[blueprint][version] = 2.1
projects[blueprint][type] = "theme"

projects[pixture_reloaded][version] = 3.2
projects[pixture_reloaded][type] = "theme"
projects[pixture_reloaded][patch][] = "http://drupal.org/files/pixelreloaded-1096218-undefined-2.patch"

projects[mix_and_match][version] = 1.6
projects[mix_and_match][type] = "theme"

projects[fbg][version] = 1.0
projects[fbg][type] = "theme"
