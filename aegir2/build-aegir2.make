core = 6.x
api = 2

projects[drupal][type] = "core"

projects[hostmaster][type] = "profile"
projects[hostmaster][download][type] = "git"
projects[hostmaster][download][url] = "http://git.drupal.org/project/hostmaster.git"
projects[hostmaster][download][branch] = "6.x-2.1"

includes[] = http://cgit.drupalcode.org/sandbox-theohawse-2157275/plain/aegir2/aegir2-contrib.make
