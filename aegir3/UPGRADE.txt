Borrowed From http://community.aegirproject.org/upgrading/manual

Be Sure To Upgrade Drush First!! Use this command from your /var/aegir directory:

drush dl drush

Then Upgrade Provision!! Use this command or similair:

drush dl provision-7.x-3.0-alpha1 --destination=$HOME/.drush

Remember to remove the provision.bak file or move it to a safe location outside of the drush folder

Clear the drush cache : drush cc drush

Before upgrading, we set a few key variables to make the process easier. You must replace the arguments those variables to match your configuration. This means replacing $AEGIR_DOMAIN by the URL of your Aegir install, $AEGIR_VERSION with the version number of the Aegir you are trying to install, and also $OLD_AEGIR_DIR with the path to the previous directory where Aegir was installed.

The directory passed to hostmaster-migrate must be an absolute path to where you want the new release to be stored.

Remember to disable the views module prior to upgrading

OLD_AEGIR_DIR=/var/aegir/hostmaster-6.x-2.1
AEGIR_VERSION=7.x-3.0-alpha1
AEGIR_DOMAIN=aegir.example.com

To upgrade your frontend, run the following commands, replacing the variables as described below:

cd $OLD_AEGIR_DIR
drush hostmaster-migrate $AEGIR_DOMAIN $HOME/hostmaster-$AEGIR_VERSION --makefile='http://cgit.drupalcode.org/sandbox-theohawse-2157275/plain/aegir3/build-aegir3.make'
