api = 2
core = 7.x

projects[pressflow][type] = "core"
projects[pressflow][download][type] = "get"
projects[pressflow][download][url] = "https://github.com/pressflow/7/tarball/master"

projects[pressflow][patch][1317338] = http://drupal.org/files/robots.txt.drupal-7.4.patch
projects[pressflow][patch][777116] = http://drupal.org/files/issues/777116-no-roles-error.patch
projects[pressflow][patch][944582] = http://drupal.org/files/d7-944582-59-do-not-test.patch

includes[] = http://cgit.drupalcode.org/sandbox-theohawse-2157275/plain/ck7/ck7-modules.make
includes[] = http://cgit.drupalcode.org/sandbox-theohawse-2157275/plain/ck7/ck7-themes.make
includes[] = http://cgit.drupalcode.org/sandbox-theohawse-2157275/plain/ck7/ck7-libraries.make
includes[] = http://cgit.drupalcode.org/sandbox-theohawse-2157275/plain/ck7/ck7-profiles.make
