api = 2
core = 7.x

projects[commerce_kickstart][type] = "profile"
projects[commerce_kickstart][download][type] = "git"
projects[commerce_kickstart][download][url] = "http://git.drupal.org/project/commerce_kickstart.git"
projects[commerce_kickstart][download][branch] = "7.x-2.18"
