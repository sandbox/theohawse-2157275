api = 2
core = 7.x

;****************************************
; Themes
;****************************************

projects[adaptivetheme][type] = theme
projects[adaptivetheme][version] = 3.2

projects[at_panels_everywhere][type] = theme
projects[at_panels_everywhere][version] = 3.x-dev

projects[ember][type] = theme
projects[ember][version] = 2.0-alpha2

projects[radix][type] = theme
projects[radix][version] = 2.0
